# NF-DBCAN

This Nextflow script is designed to process protein sequences using the DBCAN (Database for Automated Carbohydrate-active enzyme ANnotation) tool. The pipeline involves splitting a given FASTA file into smaller fragments and then applying DBCAN on each fragment in parallel.

## Requirements

- Nextflow: Make sure you have Nextflow installed on your system. You can find installation instructions [here](https://www.nextflow.io/docs/latest/getstarted.html).
- Conda: The script uses Conda to manage dependencies. Ensure Conda is available in your environment.

## Usage

Clone the repository and navigate to the script directory:

```bash
git clone https://gitlab.pasteur.fr/aghozlan/nf-dbcan.git
cd nf-dbcan
```

To run the workflow, execute the following command:

```bash
nextflow nf-dbcan.nf --in <catalogue.faa> --out <output_file> --cpus <nb_cpus> --db <dbcan_db> -w <temp_work_dir>
```

### Parameters:
- --in: Input protein sequences in FASTA format.
- --out: Output file for DBCAN results in TSV format.
- --cpus: Number of CPUs to use for parallel processing.
- --db: Path to the DBCAN database directory.
- -w: Temporary work directory for intermediate files.

### Optional Parameters:
- --block_size: Block size for splitting the input FASTA file. (Default: 70000)
- --tools: Tools used for annotation (hmmer, diamond, dbcansub, all). (Default: all).
- --help: Display usage information.

### Example:

```bash
nextflow nf-dbcan.nf --in example/example_proteome.faa --out dbcan_overview.tsv --cpus 8 --db db/
```

### Workflow Details

- run_dbcan: Runs DBCAN on each fragment in parallel.
- Workflow: Orchestrates the entire workflow, starting from splitting the input to collecting DBCAN results.

### License

This program is distributed under the terms of the GNU General Public License.