#!/usr/bin/env nextflow
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//    A copy of the GNU General Public License is available at
//    http://www.gnu.org/licenses/gpl-3.0.html

workflow.onComplete = {
    // any workflow property can be used here
    println "Pipeline complete"
    println "Command line: $workflow.commandLine"
}


workflow.onError = {
    println "Oops .. something went wrong"
}

params.help=false

def usage() {
    println("nextlfow nf-dbcan.nf --in <catalogue.faa> --out <output_file> --cpus <nb_cpus> --db <dbcan_db> -w <temp_work_dir>")
}


if(params.help){
    usage()
    exit(1)
}

params.in = "${projectDir}/example/example_proteome.faa"
params.db = "${projectDir}/db/"
params.out = "${projectDir}/dbcan_overview.tsv"
params.block_size = 70000
params.cpus = 10
params.tools = "all"


process run_dbcan {
    conda "${baseDir}/environment.yml"
    beforeScript "mkdir result"
    maxRetries 2
    cpus params.cpus
    memory '10 GB'

    input:
    path faa

    output:
    path "result/overview.txt"

    """
    run_dbcan ${faa} protein --out_dir result --hmm_cpu ${params.cpus} --tf_cpu ${params.cpus} --stp_cpu ${params.cpus} --db_dir ${params.db} --dbcan_thread ${params.cpus} --tools all  --dia_cpu ${params.cpus}
    """
}

workflow {
    infile = Channel
                .fromPath("${params.in}")
                .ifEmpty { exit 1, "Cannot find read file: ${params.in}" }
                .splitFasta(by: params.block_size, file: true)
    run_dbcan(infile) | collectFile(keepHeader: true, skip: 1, name:'result', sort: true) | subscribe { it.copyTo("${params.out}") }
}
