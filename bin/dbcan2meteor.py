import sys
import argparse
import pandas as pd
from pathlib import Path

__author__ = "Amine Ghozlane"
__copyright__ = "Copyright 2015, Institut Pasteur"
__credits__ = ["Amine Ghozlane"]
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "Amine Ghozlane"
__email__ = "amine.ghozlane@pasteur.fr"
__status__ = "Developpement"


def get_arguments():
    """Retrieves the arguments of the program.
      Returns: An object that contains the arguments
    """
    # Parsing arguments
    parser = argparse.ArgumentParser(description=__doc__, usage=
                                     "{0} -h".format(sys.argv[0]))
    parser.add_argument('-i', dest='dbcan_file', type=Path, required=True,
                        help='Path to the dbcan file.')
    parser.add_argument('-a', dest='annotation_file', type=Path, required=True,
                        default="", help='Path to lite annotation file.')
    parser.add_argument('-o', dest='output_file', type=Path, required=True,
                        help='Output file.')
    args = parser.parse_args()
    return args

def main():
    """Main program
    """
    args = get_arguments()
    dbcan_content = pd.read_csv(args.dbcan_file, delimiter="\t")
    annotation_content = pd.read_csv(args.annotation_file, delimiter="\t")
    dbcan_content_merged = dbcan_content.merge(annotation_content[["gene_id", "gene_name"]], on="gene_name", how="inner")
    dbcan_content_merged = dbcan_content_merged[["gene_id", "gene_name", "annotation"]].sort_values(by=["gene_id"])
    dbcan_content_merged = dbcan_content_merged.groupby("gene_id").head(1)
    dbcan_content_merged.to_csv(args.output_file,sep="\t", index=False)


if __name__ == '__main__':
    main()